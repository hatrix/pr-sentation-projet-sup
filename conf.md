% Projet de SUP
% Hatrix
% Samedi 2 Novembre 2013

# Un jeu vidéo ? Non !

## Connaissances
### Qu'est-ce que ça nous apporte ?
* Jeu-vidéo :
	* traitement de l'image
	* C#, XNA, SDL
	* physique
	* mathématiques
	* algorithmique
* Projet Technique :
	* algorithmique
	* architecture
	* assembleur
	* compilateur
	* VHDL, caml


## Pré-requis ?
### Pas vraiment
* Savoir lire l'anglais
* Être motivé

## TECS ?
### The Elements of Computing Systems
* bon livre
* pas cher (23$)
* ... ou pdf
* site web : nand2tetris.org
* projets
* logiciels

## Différentes parties
### Projet bien découpé (1/2)
* Hardware :
	* Architecture
	* Implémentation FPGA
	* Assembleur (langage)
* Assembleur (programme) :
	* traitement des fichiers
	* gestion des erreurs
	* sortie : binaire
* Traducteur et langage VM :
	* utilise stack et segments
	* traitement des fichiers
	* gestion des erreurs
	* optimisation
	* sortie : assembleur

### Projet bien découpé (2/2)
* Compilateur et langage haut niveau :
	* similaire à JAVA
	* compilation
	* récursivité
	* traitement de fichiers
	* gestion des erreurs avancée
* Système d'exploitation :
	* bibliothèque de fonctions
	* langage haut niveau
	
# Hardware

## FPGA ?
### Implémentation physique
FPGA : Field Programmable Gate Array

Langage : VHDL

Réelle implémentation

Clavier, écran VGA, etc

## Architecture
### Instructions, mémoires
* deux registres : D et A
* registres virtuels (RAM) :
	* R0 jusqu'à R16
	* Certains réservés
* processeur 16 bits
* opérations simples :
	* addition
	* soustraction
	* bitwise
* écran 512*256 noir et blanc

### Assembleur simple 
Mettre le nombre 42 dans la case mémoire 567 :
\newline
\newline
@42\
D=A\
@567\
M=D


# Assembleur
## Pas compliqué
### Assez facile
* prend un fichier .asm
* ressort un binaire
* instructions facilement traitables :
	* @ : adresse
	* M : mémoire[A]
	* A : registre d'adresse
	* D : registre
	* \_=\_ : affectation
	* \_[+-]\_ : opération

## Exemple
### Addition
Addition de deux nombres :
\newline
\newline
@35\
D=A\
@7\
D=D+A\
@256\
M=D

# VM Translator
## Moins facile
### Plus compliqué
* prend un fichier .vm
* ressort un .asm
* langage plus compréhensible :
	* push
	* pop
	* add
	* sub
	* mult
	* etc
* fonctions, classes
* labels
* optimisation

## Stack et segments
### Stack ?
* Revoyez votre cours d'algo : pile
* Commence à 256 dans la RAM
* @SP : prochain élément

### Segments ?
* Variables pour fonctions :
	* arguments
	* locales
* segment statique
* pointeurs : R0, R1, ...

## Exemple
### Addition en langage VM
Ajout de deux nombres dans la stack, puis addition :
\newline
\newline
push constant 35\
push constant 7\
add

### La même, traduite
Ajout de deux nombres dans la stack, puis addition :
\newline
\newline
@35, D=A, @SP, A=M, M=D, @SP, M=M+1,\
@7, D=A, @SP, A=M, M=D, @SP, M=M+1,\
@SP, A=M-1, D=M, @SP, M=M-1, A=M-1, M=M+D

# Langage haut niveau
# OS
